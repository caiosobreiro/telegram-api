FROM node:10

COPY . /app
WORKDIR /app

RUN npm i

ENTRYPOINT [ "npx", "nodemon", "src"]
