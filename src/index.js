const express = require('express');
const bodyParser = require('body-parser');

const config = require('../config.json');

if (!config) {
    console.log('config not found!');
    process.exit(1);
}

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', baseRoute);

const port = config.port;
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});

function baseRoute (req, res) {
    res.send({
        message: 'Hello World!'
    });
}
